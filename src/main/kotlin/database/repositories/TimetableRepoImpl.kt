package database.repositories

import database.models.Group
import database.models.Lesson
import database.repositories.groupRepo.GroupRepository
import database.repositories.timetableRepo.TimetableRepository
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import java.time.Instant


class TimetableRepoImpl(
    private val groupRepo: GroupRepository,
    private val lessonsRepo: TimetableRepository
) : TimetableRepo {
    override suspend fun updateGroupWithLessons(group: Group, lessons: List<Lesson>, instant: Instant) {
        newSuspendedTransaction(Dispatchers.IO) {
            val groupDAO = groupRepo.run { findByGroupNum(group.groupNum) ?: save(group) }
            lessonsRepo.run {
                deleteLessonsOf(groupDAO)
                saveFor(groupDAO, lessons)
            }
        }

    }

    override suspend fun getByGroupNum(groupNum: String): List<Lesson>? =
        newSuspendedTransaction(Dispatchers.IO) {
            val groupDAO = groupRepo.run { findByGroupNum(groupNum) }
            if (groupDAO == null) null
            else lessonsRepo.run {
                findLessonsOf(groupDAO).map { dao -> dao.asLesson() }
            }
        }

    override suspend fun getAllGroups(): List<Group> =
        newSuspendedTransaction(Dispatchers.IO) {
            groupRepo.run { getAll() }
        }

    override suspend fun getGroupsNumsStartedWith(prefix: String): List<Group> =
        newSuspendedTransaction(Dispatchers.IO) {
            groupRepo.run { getGroupsNumsStartedWith(prefix) }
        }
}