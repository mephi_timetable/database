package database.repositories.timetableRepo

import database.models.dao.GroupDAO
import database.models.dao.TimetableDAO
import database.models.Lesson
import org.jetbrains.exposed.sql.Transaction

interface TimetableRepository {
    suspend fun Transaction.deleteLessonsOf(group: GroupDAO)
    suspend fun Transaction.findLessonsOf(group: GroupDAO): List<TimetableDAO>
    suspend fun Transaction.saveFor(groupDAO: GroupDAO, lessons: List<Lesson>)

}