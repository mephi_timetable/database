package database.models.dao

import database.models.Group
import database.models.tables.GroupTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class GroupDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<GroupDAO>(GroupTable)

    var groupNum by GroupTable.groupNum
    var groupRef by GroupTable.groupRef
    var timestamp by GroupTable.timestamp

    fun asGroup() = Group(groupNum, groupRef)
}