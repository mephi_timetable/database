package database.models.tables

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.javatime.timestamp
import java.time.Instant

object TimetableTable : IntIdTable("MEPHI_TIMETABLE") {
    val group = reference("group", GroupTable).nullable()
    val discipline = varchar("discipline", 500).nullable()
    val teacher = varchar("teacher", 500).nullable()
    val place = varchar("place", 50).nullable()
    val lessonStart = datetime("lesson_start")
    val lessonEnd = datetime("lesson_end")
    val until = datetime("lesson_until").nullable()
    val repeat = varchar("repeat",10).nullable()
    val interval = integer("interval").nullable()
    val timestamp = timestamp("timestamp").clientDefault { Instant.now() }
}
