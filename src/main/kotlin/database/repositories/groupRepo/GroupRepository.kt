package database.repositories.groupRepo

import database.models.dao.GroupDAO
import database.models.Group
import org.jetbrains.exposed.sql.Transaction

interface GroupRepository {
    suspend fun Transaction.save(group: Group): GroupDAO
    suspend fun Transaction.findByGroupNum(groupNum: String): GroupDAO?
    suspend fun Transaction.getAll(): List<Group>
    suspend fun Transaction.getGroupsNumsStartedWith(prefix: String): List<Group>
}