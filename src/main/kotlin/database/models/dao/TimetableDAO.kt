package database.models.dao

import database.models.Lesson
import database.models.tables.TimetableTable
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toKotlinInstant
import kotlinx.datetime.toKotlinLocalDateTime
import kotlinx.datetime.toLocalDateTime
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class TimetableDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TimetableDAO>(TimetableTable)
    var group by GroupDAO optionalReferencedOn  TimetableTable.group
    var discipline by TimetableTable.discipline
    var teacher by TimetableTable.teacher
    var place by TimetableTable.place
    var lessonStart by TimetableTable.lessonStart
    var lessonEnd by TimetableTable.lessonEnd
    var until by TimetableTable.until
    var repeat by TimetableTable.repeat
    var interval by TimetableTable.interval
    var timestamp by TimetableTable.timestamp

    fun asLesson() =
        Lesson(
            group = group?.asGroup(),
            discipline = discipline,
            teacher = teacher,
            place = place,
            lessonStart = lessonStart.toKotlinLocalDateTime(),
            lessonEnd = lessonEnd.toKotlinLocalDateTime(),
            until = until?.toKotlinLocalDateTime(),
            repeat = repeat,
            interval = interval ?: 0,
            downloadedTime = timestamp.toKotlinInstant()
                .toLocalDateTime(TimeZone.currentSystemDefault())
        )
}