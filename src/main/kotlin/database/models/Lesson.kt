package database.models

import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
data class Lesson(
    val group: Group?,
    val discipline: String?,
    val teacher: String?,
    val place: String?,
    val lessonStart: LocalDateTime,
    val lessonEnd: LocalDateTime,
    val until: LocalDateTime?,
    val repeat: String?,
    val interval: Int,
    val downloadedTime: LocalDateTime
)