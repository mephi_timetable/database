package database.repositories.timetableRepo

import database.models.dao.GroupDAO
import database.models.dao.TimetableDAO
import database.models.Lesson
import database.models.tables.TimetableTable
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.datetime.toJavaInstant
import kotlinx.datetime.toJavaLocalDateTime
import org.jetbrains.exposed.sql.Transaction

class TimetableRepositoryImpl : TimetableRepository {
    override suspend fun Transaction.deleteLessonsOf(group: GroupDAO) {
        findLessonsOf(group).forEach { it.delete() }
    }
    override suspend fun Transaction.findLessonsOf(group: GroupDAO): List<TimetableDAO> =
        TimetableDAO.find { TimetableTable.group eq group.id }.toList()

    override suspend fun Transaction.saveFor(groupDAO: GroupDAO, lessons: List<Lesson>) {
        lessons.forEach { lesson ->
            TimetableDAO.new {
                group = groupDAO
                discipline = lesson.discipline
                teacher = lesson.teacher
                place = lesson.place
                lessonStart = lesson.lessonStart.toJavaLocalDateTime()
                lessonEnd = lesson.lessonEnd.toJavaLocalDateTime()
                until = lesson.until?.toJavaLocalDateTime()
                repeat = lesson.repeat
                interval = lesson.interval
                timestamp = lesson.downloadedTime.toInstant(TimeZone.currentSystemDefault()).toJavaInstant()
            }
        }
    }
}













