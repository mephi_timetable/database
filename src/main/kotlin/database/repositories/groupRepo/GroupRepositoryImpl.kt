package database.repositories.groupRepo

import database.models.dao.GroupDAO
import database.models.Group
import database.models.tables.GroupTable
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.datetime.toJavaInstant
import org.jetbrains.exposed.sql.Transaction

class GroupRepositoryImpl : GroupRepository {
    override suspend fun Transaction.getAll(): List<Group> =
        GroupDAO.all().map { it.asGroup() }

    override suspend fun Transaction.getGroupsNumsStartedWith(prefix: String): List<Group> =
        GroupDAO.find { GroupTable.groupNum like "$prefix%" }.map { it.asGroup() }

    override suspend fun Transaction.save(group: Group) = saveGroup(group)

    override suspend fun Transaction.findByGroupNum(groupNum: String): GroupDAO? =
        GroupDAO.find { GroupTable.groupNum eq groupNum}.firstOrNull()

    private fun saveGroup(group: Group) = GroupDAO.new {
        groupNum = group.groupNum
        groupRef = group.groupRefId
        timestamp = group.downloadedTime.toInstant(TimeZone.currentSystemDefault()).toJavaInstant()
    }

}