package database.models.tables

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.javatime.timestamp
import java.time.Instant

object GroupTable : IntIdTable("MEPHI_GROUP") {
    val groupNum = varchar("group_num", 8).uniqueIndex()
    val groupRef = varchar("group_ref", 6).uniqueIndex()
    val timestamp = timestamp("timestamp").clientDefault { Instant.now() }
}