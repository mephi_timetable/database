package database.repositories

import database.models.Group
import database.models.Lesson
import java.time.Instant

interface TimetableRepo {
    suspend fun updateGroupWithLessons(group: Group, lessons: List<Lesson>, instant: Instant)
    suspend fun getByGroupNum(groupNum: String): List<Lesson>?
    suspend fun getAllGroups(): List<Group>
    suspend fun getGroupsNumsStartedWith(prefix: String): List<Group>
}